# Example 3: Working with Multiple Sections in configparser

## Introduction

This tutorial explores managing multiple sections within a `.ini` configuration file using Python's `configparser` package. Managing multiple sections effectively is crucial for applications that need to maintain separate configurations for various modules, user roles, or operational modes.

## Why Manage Multiple Sections?

Separating configuration data into distinct sections allows for organized and modular settings management. This structure makes the configuration file easier to navigate and maintain, especially as applications grow in complexity.

## Key Concepts

### Accessing Sections
`configparser` makes it easy to access data from specific sections. Each section can be treated somewhat like a dictionary, allowing for straightforward retrieval of configuration settings.

### Checking Section Existence
Before attempting to access or modify settings in a section, it's prudent to check if the section exists. This helps avoid runtime errors and ensures that your application handles configuration data gracefully.

### Modifying Sections
`configparser` also supports dynamic modifications to the configuration, such as adding new sections or removing outdated ones. This capability is beneficial for applications that need to adapt their behavior based on runtime decisions or updates.

## Practical Application

### Configuration File Setup

The `sections.ini` file for this example includes several sections, each representing different aspects of the application settings:

```ini
[GeneralSettings]
application_name = MyApp
version = 1.4.3

[UserPreferences]
theme = dark
language = English

[DeveloperSettings]
debug_mode = true
logs_directory = /var/log/myapp

[DeprecatedSection]
stuff_we_do_not_need_anymore = old_stuff
```

### Example Usage
Here is how the configparser module is utilized to manage these sections:

```python
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)

    # Accessing settings from 'GeneralSettings'
    general_settings = config['GeneralSettings']
    print(f"Application Name: {general_settings['application_name']}")
    print(f"Version: {general_settings['version']}")

    # Verifying and managing sections
    if 'DeveloperSettings' in config:
        print("DeveloperSettings exists!")
    if 'RuntimeSettings' not in config:
        config.add_section('RuntimeSettings')
        config.set('RuntimeSettings', 'start_maximized', 'True')
    if 'DeprecatedSection' in config:
        config.remove_section('DeprecatedSection')

    # Save changes if any
    with open(file_path, 'w') as configfile:
        config.write(configfile)

load_configuration("sections.ini")
```

This script will check for the existence of certain sections, modify them as necessary, and output the settings to demonstrate how the data is accessed and manipulated.

### Expected Output
When running the provided script with the sections.ini configuration file, the console output will display the current settings from the GeneralSettings section, confirm the existence of the DeveloperSettings section, and reflect any changes made during the script's execution. Here's what the output will look like based on the script actions and the initial contents of the .ini file:

```text
Application Name: MyApp
Version: 1.4.3
DeveloperSettings exists!
```
Details of the Output:

* Application Name and Version: These lines show the values retrieved from the GeneralSettings section of the sections.ini file.
* DeveloperSettings Exists: This confirmation message indicates that the DeveloperSettings section was successfully found in the configuration file.

Additionally, since the script also involves modifying sections (adding new sections or setting new keys if they do not exist), the final state of the sections.ini file will have any changes saved back to it. For example, if the RuntimeSettings section was not initially present, it would be added with a start_maximized key set to 'True'. Similarly, if there were a section named DeprecatedSection, it would be removed.

After the example script runs, the `sections.ini` file will look like:

```ini
[GeneralSettings]
application_name = MyApp
version = 1.4.3

[UserPreferences]
theme = dark
language = English

[DeveloperSettings]
debug_mode = true
logs_directory = /var/log/myapp

[RuntimeSettings]
start_maximized = True
```

This output demonstrates how the script interacts with the configuration file, provides feedback on its actions, and confirms the effective management of settings through configparser. It’s a practical demonstration of accessing, verifying, and dynamically modifying configuration data within an application.
## The Code

The supplied files are:

- **`main.py`**: which is our primary application. It can be run directly (python main.py).
- **`copy this file to 'sections.ini'`**: contains what we want as the starting text in the `sections.ini` file. Since `main.py` demonstrates how to modify `sections.ini`, this file provides a quick and easy way to "reset" the example.
- **`sections.ini`**: is the .ini file on which `main.py` will act. `sections.ini` will be different before and after main.py executes.

## Conclusion
Understanding how to work with multiple sections in configuration files is essential for building flexible and maintainable software. By leveraging configparser to manage these sections, developers can ensure that their applications are adaptable to changes and can handle complex settings in a structured manner.