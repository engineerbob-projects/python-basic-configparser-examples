# main.py
"""
Title: Working with Multiple Sections in configparser

This tutorial covers managing multiple sections within a configuration file
using the configparser package in Python. The sections.ini file includes
several predefined sections such as GeneralSettings, UserPreferences, and
DeveloperSettings.

Key Concepts:
- Accessing Sections: Demonstrates how to read and manipulate data from
  various sections.
- Checking Section Existence: Shows how to verify whether a section exists
  within the configuration file.
- Modifying Sections: Includes adding new sections and removing existing ones.

Use Cases:
Understanding how to manage sections is vital for applications that have
modular settings or require different configurations for various user roles or
operational modes.
"""
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)

    # Access and print a specific section
    general_settings = config['GeneralSettings']
    print(f"Application Name: {general_settings['application_name']}")
    print(f"Version: {general_settings['version']}")

    # Check if a section exists
    if 'DeveloperSettings' in config:
        print("DeveloperSettings exists!")
    else:
        print("DeveloperSettings does not exist.")

    # Add a new section
    if 'RuntimeSettings' not in config:
        config.add_section('RuntimeSettings')
        config.set('RuntimeSettings', 'start_maximized', 'True')

    # Remove a section
    if 'DeprecatedSection' in config:
        config.remove_section('DeprecatedSection')

    # Save the changes back to the file
    with open(file_path, 'w') as configfile:
        config.write(configfile)

if __name__ == "__main__":
    load_configuration("sections.ini")
