# Example 4: Using Default Values in configparser

## Introduction

This tutorial demonstrates how to effectively use default values in configuration files using Python's `configparser` package. Default values, defined under the `[DEFAULT]` section of a `.ini` file, can be inherited by other sections, simplifying configuration management and ensuring consistency across different parts of an application.

## Why Use Default Values?

Utilizing default values in configuration files helps in maintaining a clean and DRY (Don't Repeat Yourself) codebase. It allows for common settings to be centralized, reducing the need for repeated definitions and making it easier to manage and update configurations.

## Key Concepts

### Default Section
Settings placed in the `[DEFAULT]` section of a `.ini` file are automatically available to all other sections. This means that any setting defined here does not need to be repeated in individual sections unless a specific override is necessary.

### Inheritance
Sections inherit settings from the `[DEFAULT]` section, making it straightforward to maintain consistent settings across various configurations. Overrides can be specified by simply providing a new value for a default setting in any section.

## Practical Application

### Configuration File Setup

Here is the content of the `default_values.ini` used in this example:

```ini
[DEFAULT]
app_version = 1.0
log_level = INFO

[Database]
db_name = sample_db
db_user = admin
# Inherits 'app_version' and 'log_level' from DEFAULT

[Server]
server_port = 8080
server_ip = 192.168.1.1
# Inherits 'app_version' and 'log_level' from DEFAULT
```

### Example Usage
The script below demonstrates how configparser handles default values and inheritance:

```python
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)

    # Access and print settings
    print("Database Section:")
    print(f"DB Name: {config['Database']['db_name']}")
    print(f"DB User: {config['Database']['db_user']}")
    print(f"App Version (inherited): {config['DEFAULT']['app_version']}")
    print(f"Log Level (inherited): {config['DEFAULT']['log_level']}")

    print("\nServer Section:")
    print(f"Server IP: {config['Server']['server_ip']}")
    print(f"Server Port: {config['Server']['server_port']}")
    print(f"App Version (inherited): {config['DEFAULT']['app_version']}")
    print(f"Log Level (inherited): {config['DEFAULT']['log_level']}")

load_configuration('default_values.ini')
```

Running this script with the provided .ini file will output the settings, showcasing how inheritance from the [DEFAULT] section simplifies configuration management.

Expected output:
```text
Database Section:
DB Name: sample_db
DB User: admin
App Version (inherited): 1.0
Log Level (inherited): INFO

Server Section:
Server IP: 192.168.1.1
Server Port: 8080
App Version (inherited): 1.0
Log Level (inherited): INFO
```
## The Code

This section provides a brief overview of the files included in this example and their roles in demonstrating the use of default values with `configparser`.

- **`main.py`** is the primary script that demonstrates how to utilize `configparser` for handling default values in configuration files. 
- **`default_values.ini`** is the configuration file on which `main.py` operates.

### How to Interact

- **Running the Script**: Execute `main.py` using Python to see how it processes the configuration settings from `default_values.ini`.
- **Experimenting**: Modify `default_values.ini` to test different scenarios, such as changing default values or adding new sections, to understand the flexibility and power of using the `[DEFAULT]` section in configuration files.



## Conclusion
Leveraging the [DEFAULT] section in .ini files to define shared settings across multiple sections not only makes the application easier to configure and maintain but also aligns with best practices in software development. This approach ensures that settings are managed centrally, reducing errors, and redundancy, and improving the modifiability of the application.