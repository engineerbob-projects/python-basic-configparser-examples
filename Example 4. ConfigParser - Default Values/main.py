# main.py
"""
Title: Using Default Values in configparser

This tutorial explores using default values defined in the [DEFAULT] section
of a .ini configuration file, which can be shared across other sections in
the file. This approach simplifies configuration management by allowing common
settings to be defined in a single location and inherited elsewhere.

Key Concepts:
- Default Section: All settings in the [DEFAULT] section are automatically
  inherited by other sections unless overridden.
- Inheritance: Shows how settings like 'app_version' and 'log_level' are used
  across multiple configurations without repetition.

Use Cases:
Default values are especially useful in applications requiring consistent
settings across multiple modules or environments, reducing errors and
redundancy.
"""
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)

    # Access default settings
    app_version = config['DEFAULT']['app_version']
    log_level = config['DEFAULT']['log_level']

    # Access specific settings and inherit defaults
    db_name = config['Database']['db_name']
    db_user = config['Database']['db_user']
    server_port = config['Server']['server_port']
    server_ip = config['Server']['server_ip']

    # Print configurations to demonstrate access and inheritance
    print("Database Section:")
    print(f"DB Name: {db_name}")
    print(f"DB User: {db_user}")
    print(f"App Version (inherited): {app_version}")
    print(f"Log Level (inherited): {log_level}")

    print("\nServer Section:")
    print(f"Server IP: {server_ip}")
    print(f"Server Port: {server_port}")
    print(f"App Version (inherited): {app_version}")
    print(f"Log Level (inherited): {log_level}")

if __name__ == "__main__":
    load_configuration("default_values.ini")
