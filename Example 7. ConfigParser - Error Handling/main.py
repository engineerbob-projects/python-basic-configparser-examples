# main.py
"""
Title: Error Handling in configparser

This tutorial illustrates how to handle common errors in configparser when
reading configuration files in Python. By using structured error handling,
applications can gracefully manage incorrect data formats and other issues
without crashing.

Key Concepts:
- ValueError Handling: Specific handling for data conversion issues, such as
  converting a non-integer string to an integer or a non-boolean string to a
  boolean.
- ConfigParser Errors: Handling errors related to file parsing and data
  retrieval, ensuring the application can report issues meaningfully and
  continue running.

Use Cases:
Error handling is critical in applications where configurations are dynamic
or user-provided, ensuring that incorrect settings do not disrupt the
application's functionality.
"""
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    try:
        config.read(file_path)

        # Attempt to read an integer value
        try:
            font_size = config.getint('Settings', 'font_size')
            print(f"Font Size: {font_size}")
        except ValueError:
            print("Error: Font size must be an integer.")

        # Attempt to read a boolean value
        try:
            auto_save = config.getboolean('UserPreferences', 'auto_save')
            print(f"Auto Save: {auto_save}")
        except ValueError:
            print("Error: Auto save must be a boolean (true/false).")

    except configparser.Error as e:
        print(f"Failed to read configuration file: {e}")

if __name__ == "__main__":
    load_configuration("error_handling.ini")
