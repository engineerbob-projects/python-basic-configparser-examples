# Example 6: Advanced Parsing Techniques with configparser

## Introduction
This example delves into advanced features of Python's `configparser` module, demonstrating how to extend its capabilities to meet specific application needs. We explore custom interpolation of values, enhancing configuration flexibility, and using custom converters to handle dynamic data types.

## Key Concepts Covered
- **Custom Interpolation**: Demonstrated by the use of `%(...)s` syntax, allowing settings to dynamically reference other settings within the same configuration file.
- **Extending ConfigParser**: By subclassing `configparser.ConfigParser`, we introduce customized behavior such as evaluating expressions for integer conversion, shown in the `ExtendedConfigParser` class.
- **Custom Converters**: Implemented through methods like `getint` in the `ExtendedConfigParser` which handle more complex data conversions beyond the standard data types.

## Use Cases
Advanced parsing techniques are useful in complex applications where configuration settings are interdependent or need to adapt based on other configurations or environmental factors.

## Understanding the `%(...)s` Syntax for Interpolation

### How It Works:
- **Syntax**: The `%(...)s` indicates a placeholder for interpolation within the string. The text inside the parentheses corresponds to the key of another configuration setting that can be dynamically inserted at this location.
- **Purpose**: This allows a value in one setting to be included dynamically in another, enabling flexible and dynamic configurations.

### Example Use in `advanced_parsing.ini`:
```ini
[DEFAULT]
root_dir = /usr/local

[Paths]
log_dir = %(root_dir)s/logs
backup_dir = %(root_dir)s/backup
```

In the configuration file above, %(root_dir)s within log_dir and backup_dir settings dynamically inserts the value of root_dir from the DEFAULT section. Therefore log_dir would evaluate to "/usr/local/logs", and backup_dir would be "/usr/local/backup". In this instance interpolation allowed the configuration of paths (log_dir, and backup_dir) based on other defined settings (root_dir).

## Example

The script `main.py` demonstrates several advanced techniques for handling configurations in Python using the `configparser` module. Here's a detailed breakdown of its operations:

1. **Subclass ConfigParser**: The `ExtendedConfigParser` class extends the basic functionality of `configparser.ConfigParser`. It customizes the `getint` method to handle not just direct integer conversions but also the evaluation of expressions. This allows for more dynamic and adaptable configuration settings.

2. **Read Configuration File**: The script begins by reading the configuration settings from `advanced_parsing.ini`. This file includes default settings and other settings that utilize variable interpolation for dynamically constructing paths and other configuration values.

3. **Interpolate Settings**: The script demonstrates dynamic interpolation by constructing paths for log and backup directories based on a root directory specified in the configuration file (`%(root_dir)s`). This shows how settings can reference other settings within the same file, enhancing flexibility and reducing redundancy.

4. **Custom Integer Conversion**: Using the overridden `getint` method of `ExtendedConfigParser`, the script converts the `port` setting from the `[Server]` section. This method can evaluate both direct integers and expressions, providing robust error handling and conversion capabilities, which is particularly useful for settings that may need to be calculated or derived at runtime.
> **Explanation of `eval()`**: The `eval()` function evaluates the specified expression, if the expression is a legal Python statement. It allows dynamic execution of Python code which can be very powerful but also opens up security risks if not used cautiously. In this example, it is used

5. **Output Configuration Values**: After processing the configuration settings, the script prints the resolved values for the log directory, backup directory, and server port. These outputs demonstrate the practical application of custom interpolation and extended conversion functionality in real-world scenarios.

6. **Handling Errors**: The script includes error handling for potential issues during interpolation and value conversion. This ensures that the script can gracefully manage and report configuration errors, which is critical for maintaining reliability in production environments.

By enhancing the default capabilities of `configparser`, this example illustrates how applications can manage complex configurations that dynamically adapt based on other settings or runtime conditions. This approach not only makes the configuration management more flexible but also more powerful, catering to the sophisticated needs of modern software applications.


### Python Code (main.py)
```Python
import configparser

class ExtendedConfigParser(configparser.ConfigParser):
    def getint(self, section, option, fallback=None):
        """Custom integer converter that handles expressions."""
        value = self.get(section, option, fallback=fallback)
        try:
            return int(eval(value))
        except (NameError, SyntaxError):
            return int(value)

def load_configuration(file_path):
    config = ExtendedConfigParser()
    config.read(file_path)

    print("Accessing paths using interpolation:")
    log_dir = config['Paths']['log_dir']
    backup_dir = config['Paths']['backup_dir']
    print(f"Log directory: {log_dir}")
    print(f"Backup directory: {backup_dir}")

    print("Using custom integer conversion for server port:")
    try:
        port = config.getint('Server', 'port')
        print(f"Server port: {port}")
    except configparser.InterpolationError as e:
        print(f"Error in interpolation: {e}")
    except ValueError as e:
        print(f"Error in value conversion: {e}")

if __name__ == "__main__":
    load_configuration("advanced_parsing.ini")
```

### The configuration file

Configuration File (advanced_parsing.ini):
```ini
[DEFAULT]
root_dir = /usr/local

[Paths]
log_dir = %(root_dir)s/logs
backup_dir = %(root_dir)s/backup

```ini
[DEFAULT]
default_port = 8080

[Paths]
root_dir = /usr/local
log_dir = %(root_dir)s/logs
backup_dir = %(root_dir)s/backup

[Server]
host = localhost
port = %(default_port)s
```
Expected Output
The script will output paths for log and backup directories interpolated from the root directory, and attempt to convert the server port from a string to an integer, demonstrating dynamic configuration management:

```Text
Log directory: /usr/local/logs
Backup directory: /usr/local/backup
Server port: 8080
```

## The Code

This project consists of the following files, which together demonstrate the advanced parsing capabilities of the `configparser` module:

- **`main.py`**: This Python script is the core of the example. It includes the `ExtendedConfigParser` class that subclasses `configparser.ConfigParser`. The class is customized to enhance integer conversion capabilities, allowing for evaluation of expressions and more robust data handling. The script uses this custom parser to load, parse, and output configurations from an `.ini` file, demonstrating practical applications of advanced features such as custom interpolation and converters.

- **`advanced_parsing.ini`**: The configuration file used by `main.py`. It defines settings that include default values, paths that use interpolation

## Conclusion
This example underscores the advanced capabilities of Python's configparser module, showcasing how it can be tailored to meet specific, complex application needs. Through subclassing and custom converters, the module provides powerful tools for developers needing to implement sophisticated configuration management solutions in their software projects.
