# main.py
"""
Title: Advanced Parsing Techniques with configparser

This tutorial delves into advanced features of the configparser package in
Python, demonstrating how to extend its capabilities to meet specific
application needs. We explore custom interpolation of values, enhancing the
configuration flexibility, and using custom converters to handle dynamic data
types.

Key Concepts:
- Custom Interpolation: Allows settings to reference other settings within
  the same configuration file, enabling dynamic configurations.
- Extending ConfigParser: By subclassing configparser.ConfigParser, we
  introduce customized behavior such as evaluating expressions for integer
  conversion.
- Custom Converters: Implementing methods to handle more complex data
  conversions and conditions.

Use Cases:
Advanced parsing techniques are useful in complex applications where
configuration settings are interdependent or need to adapt based on other
configurations or environmental factors.
"""
import configparser

class ExtendedConfigParser(configparser.ConfigParser):
    def getint(self, section, option, fallback=None):
        # Custom integer converter that can handle expressions
        value = self.get(section, option, fallback=fallback)
        try:
            return int(eval(value))
        except (NameError, SyntaxError):
            return int(value)

def load_configuration(file_path):
    config = ExtendedConfigParser()
    config.read(file_path)

    # Accessing paths using interpolation
    log_dir = config['Paths']['log_dir']
    backup_dir = config['Paths']['backup_dir']
    print(f"Log directory: {log_dir}")
    print(f"Backup directory: {backup_dir}")

    # Using custom integer conversion for server port
    try:
        port = config.getint('Server', 'port')
        print(f"Server port: {port}")
    except configparser.InterpolationError as e:
        print(f"Error in interpolation: {e}")
    except ValueError as e:
        print(f"Error in value conversion: {e}")

if __name__ == "__main__":
    load_configuration("advanced_parsing.ini")
