# Example 5: Writing Configurations in configparser

## Introduction
This example demonstrates how to use Python's configparser module to modify, add, and save configurations to an .ini file. The ability to dynamically change configuration settings is particularly useful for applications that need to adjust their behavior based on user preferences or operational conditions.

## Key Concepts Covered
* Modifying Existing Settings: Update existing configuration values directly by assigning new values to keys.
* Adding New Settings and Sections: Introduce new configuration settings and sections to the .ini file using methods like add_section() and direct assignments.
* Saving Changes: Save the modified configurations back to the .ini file using the write() method, ensuring changes persist.

## Use Cases
Configurations are often used in applications to manage operational parameters that can change, such as user interface preferences, application settings, or environment-specific variables. This example is crucial for developing flexible software that can easily adapt to different user environments or preferences.

## Example

In typical applications, updated configurations will overwrite the original configuration file. However, for educational purposes and to allow for repeated experimentation without losing the original settings, this example writes the updated configurations to a new file, `updated_config.ini`. This approach helps demonstrate the changes to the original configuration clearly and preserves the original `.ini` file for further use or additional tests.

### Description
The Python script `main.py` demonstrates the use of the `configparser` module to modify, add to, and save changes to configuration settings in an `.ini` file. Here’s what the script does:

1. **Load Configuration**: The script starts by loading the existing settings from `update_config.ini`.
2. **Update Settings**: It modifies the `font_size` under the `[Settings]` section and the `auto_save` setting under `[UserPreferences]`.
3. **Add New Settings**: A new setting, `notification_sound`, is added to the `[UserPreferences]` section.
4. **Create New Section**: A completely new section titled `[Accessibility]` is added with a setting of `high_contrast`.
5. **Save New Configuration**: Instead of overwriting the original configuration file, the script writes the updated settings to a new file, `updated_config.ini`, preserving the original settings for future use and experimentation.

### Python Code (main.py)
```python
import configparser

def modify_and_save_configuration(original_file_path, new_file_path):
    config = configparser.ConfigParser()
    config.read(original_file_path)

    print("Updating configurations...")

    # Modify existing settings
    config['Settings']['font_size'] = '14'  # Changing font size
    print("Updated font size to 14.")
    config['UserPreferences']['auto_save'] = 'false'  # Changing auto save preference
    print("Set auto_save to false.")

    # Add new settings in existing section
    config['UserPreferences']['notification_sound'] = 'on'
    print("Added notification_sound setting to UserPreferences.")

    # Add a new section with some settings
    if 'Accessibility' not in config:
        config.add_section('Accessibility')
        config['Accessibility']['high_contrast'] = 'enabled'
        print("Added new section Accessibility with high_contrast enabled.")

    # Save the updated configuration to a new file
    with open(new_file_path, 'w') as configfile:
        config.write(configfile)
    print(f"Configuration updates saved to {new_file_path}")

modify_and_save_configuration("update_config.ini", "updated_config.ini")
```

### Original Configuration File (update_config.ini)
```ini
[Settings]
font_size = 12
font_style = bold
theme_color = dark

[UserPreferences]
auto_save = true
language = English
```

### Expected Output
* Font size in the [Settings] section updated from 12 to 14.
* Auto-save preference in the [UserPreferences] section changed from true to false.
* New setting notification_sound = on added under [UserPreferences].
* New section [Accessibility] added with a setting high_contrast = enabled.

### Updated Configuration File (updated_config.ini)
The new file will reflect the above changes. To view the changes, you would look at the contents of updated_config.ini after running the script.

```ini
[Settings]
font_size = 14
font_style = bold
theme_color = dark

[UserPreferences]
auto_save = false
language = English
notification_sound = on

[Accessibility]
high_contrast = enabled
```
## The Code

This project consists of the following files:

- **`main.py`**: Contains the Python code that uses `configparser` to read, modify, add, and save configuration settings.
- **`update_config.ini`**: The original configuration file that contains default settings for the application.
- **`updated_config.ini`**: The new configuration file where updated and new settings are saved after running `main.py`. This file contains the changes made by the script.

## Conclusion
This example shows the power and flexibility of the configparser module for managing application settings. It is a vital tool for developers needing to maintain and update software configurations dynamically.

This document provides a comprehensive guide to understanding how the example script works, what changes it makes, and how those changes are reflected in the configuration file.
