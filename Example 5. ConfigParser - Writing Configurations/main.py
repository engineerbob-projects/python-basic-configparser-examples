# main.py
"""
Title: Writing Configurations Using configparser

This tutorial covers how to modify and write configurations using the
configparser package in Python. We demonstrate how to change existing 
settings, add new settings, and even introduce new sections to a .ini
file, followed by saving these changes back to the file.

Key Concepts:
- Modifying Existing Settings: Update values directly by assigning new values
  to keys.
- Adding New Settings and Sections: Use methods like `add_section()` and
  direct assignments to expand the configuration file.
- Saving Changes: Use the `write()` method of configparser to save changes
  back to the .ini file, effectively updating it.

Use Cases:
This capability is crucial for applications that need to adapt their 
behavior based on user preferences or operational conditions that may
change over time.
"""
import configparser

def modify_and_save_configuration(original_file_path, new_file_path):
    config = configparser.ConfigParser()
    config.read(original_file_path)

    print("Updating configurations...")

    # Modify existing settings
    config['Settings']['font_size'] = '14'  # Changing font size
    print("Updated font size to 14.")
    config['UserPreferences']['auto_save'] = 'false'  # Changing auto save preference
    print("Set auto_save to false.")

    # Add new settings in existing section
    config['UserPreferences']['notification_sound'] = 'on'
    print("Added notification_sound setting to UserPreferences.")

    # Add a new section with some settings
    if 'Accessibility' not in config:
        config.add_section('Accessibility')
        config['Accessibility']['high_contrast'] = 'enabled'
        print("Added new section Accessibility with high_contrast enabled.")

    # Save the updated configuration to a new file
    with open(new_file_path, 'w') as configfile:
        config.write(configfile)
    print(f"Configuration updates saved to {new_file_path}")

    # To update the original file, uncomment the following lines:
    # with open(original_file_path, 'w') as configfile:
    #     config.write(configfile)
    # print(f"Configuration updates saved back to {original_file_path}")

if __name__ == "__main__":
    modify_and_save_configuration("update_config.ini", "updated_config.ini")
