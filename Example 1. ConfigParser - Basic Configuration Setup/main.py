# main.py
"""
Title: Basic Configuration Setup Using configparser in Python

In this tutorial, we explore the basic usage of the configparser
package to read configuration settings from a .ini file. This
example includes a settings.ini file, which contains settings
under 'Settings' and 'UserPreferences' sections. 

The read_config.py script demonstrates how to load this
configuration file, access different sections, and retrieve
configuration values.

Key Concepts:
- .ini File Structure: Sections are denoted by square brackets [],
  and each section contains key-value pairs for configuration.
- Reading Configurations: The ConfigParser object reads the .ini
  file and provides methods to access the data.
- Accessing Sections and Values: You can access the settings
  directly by referencing the section name and the key.

This setup is useful for applications that need a simple and
manageable way to handle user preferences and other settings.
"""
import configparser

def load_configuration(file_path):
    # Create a ConfigParser object
    config = configparser.ConfigParser()
    # Read the configuration file
    config.read(file_path)

    # Access the settings section
    settings = config['Settings']
    user_prefs = config['UserPreferences']

    # Print configurations to demonstrate access
    print(f"Font Size: {settings['font_size']}")
    print(f"Font Style: {settings['font_style']}")
    print(f"Theme Color: {settings['theme_color']}")
    print(f"Auto Save: {user_prefs['auto_save']}")
    print(f"Language: {user_prefs['language']}")

if __name__ == "__main__":
    load_configuration("settings.ini")
