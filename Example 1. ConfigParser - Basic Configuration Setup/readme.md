# Example 1: Basic Configuration Setup in configparser

## Introduction

Configuration files are essential for applications that require flexibility in their behavior without changing the code. They allow settings to be modified without the need to recompile or redeploy the application. This tutorial demonstrates the basic use of the `configparser` package to manage application settings through an `.ini` file.

## Why Use `.ini` Files?

`.ini` files provide a structured format to store configuration settings. This format supports:
- **Sections**: Categorized settings, making it easier to manage and read.
- **Key-Value Pairs**: Simple representation of configuration data.

Using `.ini` files helps in several ways:
- **Separation of Code and Configuration**: Keeps the application code clean and focused on functionality rather than configuration details.
- **Ease of Modification**: Non-developers, such as system administrators or support teams, can change application behavior without touching the codebase.
- **Environment Adaptation**: Different environments (development, testing, production) can have their own configurations without code changes.

## Best Practices and Design Principles

### Single Responsibility Principle (SRP)

By separating configuration from business logic, `configparser` helps adhere to SRP — a principle that promotes each module or class to have responsibility over a single part of the functionality provided by the software, and it should encapsulate that part. Managing settings through `.ini` files means that the code does not directly handle the logic for loading and parsing these settings.

### Don't Repeat Yourself (DRY)

Configuration files also help in achieving the DRY principle by centralizing common settings. Instead of scattering database connection strings or API keys across the application, they are defined once in the configuration file. This reduces redundancy and minimizes the risk of inconsistencies.

## Practical Application

### File Structure

An `.ini` file is composed of sections, each defined by a header in square brackets `[]`, with key-value pairs under each section. This structure makes it intuitive to locate and understand settings.

In the presented example the settings.ini file contains:
```ini
[Settings]
font_size = 12
font_style = bold
theme_color = dark

[UserPreferences]
auto_save = true
language = English
```

### Loading and Accessing Configurations - Example Usage
The ConfigParser object reads the .ini file, and settings are accessed by specifying section names and keys. This modular access promotes a clean and organized way to retrieve configuration values.

Here's how the `configparser` module is used in a script to load and use settings:

```python
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    config.read(file_path)
    settings = config['Settings']
    print(f"Font Size: {settings['font_size']}")

load_configuration("settings.ini")
```
Running that example would yield a console output:
```text
Font Size: 12
```

## The Code
- **`main.py`**: Contains the code for the example.
- **`setting.ini`**: Is the `.ini` file upon which `main.py` operates.

## Conclusion
Using configparser and .ini files for managing application settings not only makes the application easier to configure and maintain but also aligns with essential software engineering principles. This approach enhances the modifiability of the application and helps in managing complexity as the application grows.