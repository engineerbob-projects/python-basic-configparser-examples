# Example 2: Handling Different Data Types in configparser

## Introduction

Managing configuration settings often involves handling various data types effectively. This tutorial showcases how to use Python's `configparser` package to read and manage different data types from `.ini` configuration files. We focus on integers, floats, and booleans, each requiring specific methods to read and validate.

## Why Manage Data Types in Configuration Files?

Data types are crucial in configuration management because they determine how settings are interpreted and used within the application. For instance, numerical values might be used for setting limits, timeouts, or defining operational parameters, while booleans could enable or disable features dynamically.

## Key Concepts

### Data Type-Specific Methods
`configparser` enhances flexibility and reliability in configuration management by providing methods like `getint()`, `getfloat()`, and `getboolean()`. These methods ensure that values are automatically converted to the appropriate type:

- **`getint()`**: Converts the configuration value to an integer.
- **`getfloat()`**: Converts the configuration value to a float.
- **`getboolean()`**: Converts the configuration value to a boolean. It recognizes 'true', 'yes', 'on', and '1' as `True`; 'false', 'no', 'off', and '0' as `False`.

### Handling Comments
Comments in `.ini` files are usually marked by semicolons (`;`) or hashes (`#`). `configparser` can be configured to recognize these prefixes and ignore them during parsing, ensuring that only the actual setting values are read.

## Practical Application

### Configuration File Setup

Here is the content of the `data_types.ini` used in this example:

```ini
[Metrics]
refresh_rate = 60   # Refresh rate as an integer
timeout = 2.5       # Timeout in seconds as a float

[Features]
debug_mode = yes    # Boolean value for debug mode
```

### Example Usage
The script demonstrates how to load and parse different data types from the configuration file effectively:

```python
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser(comment_prefixes=('#', ';'))
    config.read(file_path)

    # Accessing and converting data types
    refresh_rate = config.getint('Metrics', 'refresh_rate')
    timeout = config.getfloat('Metrics', 'timeout')
    debug_mode = config.getboolean('Features', 'debug_mode')

    print(f"Refresh Rate: {refresh_rate}")
    print(f"Timeout: {timeout}")
    print(f"Debug Mode: {debug_mode}")
  
load_configuration("data_types.ini")
```

Running this script with the provided .ini file will output the correctly typed values, demonstrating how configparser handles each type.  

The expected output:
```text
Refresh Rate: 60 (Type: int)
Timeout: 2.5 (Type: float)
Debug Mode: True (Type: bool)
```
## The Code
- **`main.py`**: Contains the code for the example.
- **`date_types.ini`**: Is the `.ini` file upon which `main.py` operates.


## Conclusion
Using configparser to handle different data types in configuration files allows applications to be more dynamic and robust. It enables developers to specify settings in a way that is easy to modify and understand, aligning with best practices in software development for maintainability and scalability. This approach not only makes the application adaptable to changes but also ensures that data types are correctly enforced, reducing runtime errors and improving overall reliability.