# main.py
""""
Title: Handling Different Data Types with configparser in Python

This tutorial demonstrates how to read and handle different
data types from a .ini configuration file using the 
configparser package. The data_types.ini file includes settings 
for refresh rates, timeouts, and debug modes, each stored in 
different data types: integers, floats, and booleans.

Key Concepts:
- Data Type-Specific Methods: configparser provides getint(), 
  getfloat(), and getboolean() to read values as specific data 
  types.
- Config File Setup: The ini file contains comments (following 
  semicolons) that do not affect configuration but provide 
  additional information.

Use Cases:
Handling various data types is crucial for applications where 
configuration settings need precise data handling, such as in 
timing operations or feature toggling based on user settings.
"""
import configparser

def load_configuration(file_path):
    config = configparser.ConfigParser()
    # Initialize ConfigParser with comment prefixes recognized
    config = configparser.ConfigParser(comment_prefixes=('#', ';'))
    config.read(file_path)

    metrics = config['Metrics']
    features = config['Features']

    # Function to strip comments and extra whitespace
    def clean_value(value):
        # Split on any known comment characters and strip whitespace
        return value.split('#')[0].strip().split(';')[0].strip()

    # Read integer, float, and boolean data types using cleaned values
    refresh_rate = int(clean_value(metrics['refresh_rate']))
    timeout = float(clean_value(metrics['timeout']))
    # Clean the boolean value before converting it
    debug_mode_raw = clean_value(features['debug_mode'])
    debug_mode = debug_mode_raw.lower() in ['true', '1', 'yes', 'on']

    # Print the values with their types to demonstrate
    print(f"Refresh Rate: {refresh_rate} (Type: {type(refresh_rate).__name__})")
    print(f"Timeout: {timeout} (Type: {type(timeout).__name__})")
    print(f"Debug Mode: {debug_mode} (Type: {type(debug_mode).__name__})")

if __name__ == "__main__":
    load_configuration("data_types.ini")
